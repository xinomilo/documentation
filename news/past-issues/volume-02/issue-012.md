# Devuan Weekly News Issue XII

__Volume 002, Week 7, Devuan Week 12__

https://git.devuan.org/Envite/devuan-weekly-news/wikis/past-issues/volume-002/issue-012


## Editorial

__Hit a Wall?  Here's a Ladder__

An uneasy wind blew fetid laments of arrogance and ivory towering.

These accusations are plain wrong.  They come as voices from people
invited to a cooking party sitting on their asses and complaining to
the host who just arrived from the market that the meal is not ready.
If you see a wall, you should get up from your chair and move around
it.

OK, the site sucks, the absence of logo sucks, the release is not
ready, Winter is too cold, Summer is too hot, etc.  There's plenty to
complain about.  But if you look around, you'll see a vastness of
empty space with work to do that's just waiting for you to pick it up
and make it nice.

If _you lack time_, the [Devuan Weekly News][wiki] is saving you some
by pointing to what's being said on the mailing list.  It's volunteer
work, and you're welcome to give a hand.  As the list traffic grows,
more attention is required to read everything and get to the point.
Every week.

If _you have time_, you can [explore the
Gitlab](https://git.devuan.org/explore) and see what's cooking.  You
can even register an account and chime in, opening issues, solving
issues, meeting people, and joining teams.

Here's from the [project's wiki][1]:

> During the initial phase, until 1.0 is released, we shall follow a
> very simple conceptual line: the more you do, the stronger your
> voice on decisions.

(Hint: complaining and bikeshedding are not "doing")

.-hellekin


## Last Week in Devuan

### [About separate mailing lists][2]

The first issue to track this week started in DWN XI, where Noel
"Envite" raised the question of separating the single DNG list (this
one) into a users list and a dev list. Steve Litt suggested that maybe
the line should be drawn differently, like code Vs. philosophy, which
was effectively in line with Envite's proposal. Others suggest that it
is not the moment, or that users should be aware of what developers
do. The thread also had its own amount of litter.

### [Community polls on Devuan design][3]

The thread about the logo poll saw an announce of partial results from
Jaromil as well as a recommendation to the design team about keeping
these results in mind when designing. It seems this whole thread created
some confusion, as hellekin points out later.

The DWN team (currently Noel "Envite" and hellekin) wants to state that:

* Our opinions count exactly the same as any other opinion.
* We try to provide a neutral view of what is happening on Devuan, but
  we positively do not refrain from shedding our own light on it.
* We welcome you to help us, and cast your own light as well.
* We do not endorse any logo proposal.
* We think democracy (and polls) are not always the best solution,
  specially when they can cause discussions on the [roof of a
  bikeshed][4].
* We think that a dedicated visual design team is a good idea, and
  that it should be a part of, and fully attentive to the community.

In summary: please keep Devuan Jessie going on, whatever we use for
the roof of the bikeshed (or as the distribution logo).

### [Raspberry Pi 2][5]

Robert Storey highlights the newly released Raspberry Pi 2, which
could be used to test ARM support for Devuan. Jaromil informed about
the possibility of selling Devuan preinstalled Raspberry Pi boxes. Wim
reported that Raspberry Foundation will use Debian Jessie with systemd
included. We also got news about ARM machines bought by Devuan.

### [Kali Linux][6]

The previous thread offspringed this one about whether Kali Linux does
actually have systemd. The consensus (trolls permitting) is that it
doesn't in the standard installation.

### Bastille Linux

The discussion about Bastille Linux keeps going on with quite a high
trolling level (it is not an error there is no link here). If somebody
deserves to be quoted, he is william moss, [throwing some sanity
in][7].

### [Guidelines][8]

Mitt requests guidelines about trademarks and non-free stuff. Our
hellekin notes that the current focus is on providing a systemd-free
Jessie, with almost no other changes.

### [*dev and screen resolution][9]

SteveT asks if `vdev` could not change screen resolution. The various
answers point that no `*dev` actually does that, but may delegate the
functionality to a module that does on its own.

### [depinit][10]

Jonathan Wilkes indicates `depinit`, yet another alternate init
system.

### [Has modern Linux lost its way?][11]

Nate Bargmann started this week's most prolific thread by pointing to
a blog post by John Goerzen in which the issue of "modern Linux" being
difficult to administer and troubleshoot is discussed.

Article and followup by John are [here][12] and [here][13].

The thread has a fair amount of thanks posts and its dose of troll
posts. In fact, the list people seems to mostly agree on the points,
and in particular that servers should not be forced to use solutions
meant for desktops. Discussion seems to revolve around complexity in
general and not about systemd in particular. Citing Steve Litt:

> I think systemd is just the worst of many entangled monolithic
> monsters, and is a symptom of the true problem.

One branch of the discussion teached [how to configure wifi without
network-manager][14], kindly provided by Didier Kryn, with
contributions by others, specially Isaac Dunham. Hendrik Boom, on its
turn, explained to substitute Network-Manager by wicd.

Another branch discussed again static devfs, and sg_map.

Yet another branch discussed diversity and how it can be a strenght
and a weakness. Vince Mulhollon pointed that the problem is in the
"obese DEs".

### [Programming languages][15]

From the thread about "modern Linux" raises again the discussion about
programming languages, where hendrik passes some in review and
remembers that we agreed to use whatever is currently on Debian.
Didier added:

> Dear developpers and maintainers, please continue providing us with 
> applications written in the language you prefer.

with the important point though of not forcing people if what you
write is an API.

### [Weekly developer status update][16]

The previous thread itself mutated into a mild complaint about the dev
team being somehow isolated from the DNG list. This caused Jude Nelson
to get the bullet with the tooth and write a status update, intended
to be weekly. DWN recommends reading it (e.g. from the link at the top
of this section).

### [Towards systemd-free packages][17]

Last week's thread goes on with indications from Jaromil about how to
use the [SDK][sdk], news regarding `udev` about to be replaced by
`eudev` or `vdedv`, Rob Owens looking towards `udevil`, Isaac Dunham
towards `mdev`, and Jaromil stating that the most probable course of
action is to keep `udev`, but just by now, in order to have Devuan
Jessie out.

### [Linux 3.20 or 4.0 and KDBUS][18]

joerg reports that Linux 3.20 may be renumbered as 4.0 because it
includes some new features, among which there is 'Live kernel patching
support via KDBUS'. He fears this makes the kernel depend on systemd.

Jude Nelson explains that kdbus just moves messages, and that while
its userspace implementation is actually on systemd, it can be made
out of it as well.

### [Keep Close to Debian][19]

Luke Kenneth Casson Leighton raised an important concern.  Two actually:

* Will Devuan keep pulling packages from Debian or will it be a
  complete fork?

* Will Devuan keep package compatibility with Debian so that they can
  be merged back into Debian?

It's hard to tell, argues KatolaZ, who's expecting a "functioning and
fuss-free GNU/Linux, which I can tinker with as [I] like".

Both questions have [some elements of answer in the wiki][20]:

> "Devuan is born for a simple goal: having a systemd-free debian
> jessie to preserve freedom choice on init and decoupling between
> init and the rest of the system [...] initially, it will NOT be a
> complete fork, but just a complete infrastructure to distribute a
> personalized version of debian jessie, testing and unstable where
> some packages from us will be pinned up on top of the debian
> repositories..."

Then we hear fiery tales of bridge burning, the happiness of [the
catharsis and the bizarre][^esr], and a recurring question: **yes, or
no?** Is it a fork, or is it a spin?  Luke Leighton brings some
welcomed light about the context of these questions: yes, Devuan can
choose to be mergeable with Debian and should not discard the
possibility of a future merge.  He insists that "it would be
**desirable** for a future merge to actually happen."  Devuan, he
adds, "could be a 'testing ground' for radical ideas such as shredding
systemd, which Debian could not possibly do without a massive amount
of risky disruption."

The keyword here is **continuity**: whatever happens with Devuan,
people will use it if they can switch to it from Debian without an
itch--which at this point can happen, as some will need to be
scratched sooner or later.

Devuan can be welcoming to existing Debian Maintainers and should be,
thinks Hendrik Boom, as the probability of Devuan needing the
collaboration of Debian Maintainers only grows with the growth of
Devuan.

Luke: "if you try to go too far, you will overwhelm the team with the
amount of work that will need to be done".  He recommends that the
differences between Devuan and Debian be kept to a minimum.

He wants "to have a word with the systemd team. i'm going to advocate
to them that they make libsystemd0 be a dynamically-loaded runtime
library", which Gravis wants to listen to, for the lulz.

TJ Duchene reminds that "Devuan was created because in our eyes, the
Debian process failed under its own weight."  He calls for not looking
back.  We'll probably hear more about this topic next week!

[^esr]: _The Catharsis and the Bizarre_, a non-existent book by Eric S. Raymond.

### [About Devuan's audience][21]

Didier Kryn wonders if Devuan's intended audience and current
community is that of server admins only or users that want to "own
their desktops" should be taken into account as well. The question is
not yet settled by list consensus, while people explain why they are
on Devuan's ship.

Gravis tells us that he is working in a new security paradigm using
POSIX means.

### Valentine's Day

Franco (and we all) received a Valentine's Day gift from Jaromil. By
explicit request it is not linked here, since it is a pre-alpha
version. Svante Signell reports that it runs in quemu-kvm currently.

In a related, but different thread, KatolaZ wrote the instructions to
make it run in qemu. Steve Litt reminds us all of the importance of
good documentation, due to a question about qemu.


## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
Do you want to continue reading Devuan Weekly News? Please help us:

* Join us at IRC: #devuan-news at freenode
* Contribute by resuming threads for the [upcoming issue XIII][upcoming]

Thanks for reading us. See you next week, time permitting.

DWN Team
* Noel, "er Envite" (Main writer, polisher)
* hellekin (Editorial writer, co-writer, markup master)
* golinux (typo spotter)


[sdk]: https://git.devuan.org/devuan/devuan-sdk "devuan-sdk repository"

[1]: https://git.devuan.org/devuan/devuan-project/wikis/DecisionalStructure "Decisional Structure"
[2]: https://lists.dyne.org/lurker/message/20150211.165903.540ad782.en.html "About separate mailing lists"
[3]: https://lists.dyne.org/lurker/message/20150212.103910.150718fb.en.html "Community polls on Devuan design"
[4]: https://en.wikipedia.org/wiki/Parkinson%27s_law_of_triviality "Parkinson's Law of Triviality"
[5]: https://lists.dyne.org/lurker/message/20150210.081812.2f48a580.en.html "Raspberry Pi 2"
[6]: https://lists.dyne.org/lurker/message/20150211.043410.9b2530da.en.html "Kali Linux has systemd?"
[7]: https://lists.dyne.org/lurker/message/20150211.195124.df54d5e3.en.html "Bastille Linux"
[8]: https://lists.dyne.org/lurker/message/20150211.201217.65eedf27.en.html "Guidelines"
[9]: https://lists.dyne.org/lurker/message/20150212.145008.3cb4b650.en.html "Nice feature for vdev"
[10]: https://lists.dyne.org/lurker/message/20150213.032713.14b8891f.en.html "depinit"
[11]: https://lists.dyne.org/lurker/message/20150211.162520.a7b93e47.en.html 'John Goerzen asks, "Has modern Linux lost its way?"'
[12]: http://changelog.complete.org/archives/9299-has-modern-linux-lost-its-way-some-thoughts-on-jessie "John Goerzen article"
[13]: http://changelog.complete.org/archives/9304-reactions-to-has-modern-linux-lost-its-way-and-the-value-of-simplicity "John Goerzen followup"
[14]: https://lists.dyne.org/lurker/message/20150212.170243.9c454504.en.html "WiFi configuration without Network-Manager"
[15]: https://lists.dyne.org/lurker/message/20150212.213629.99495c09.en.html "Programming languages again"
[16]: https://lists.dyne.org/lurker/message/20150214.214455.b5a0e3d5.en.html "Weekly developer status update"
[17]: https://lists.dyne.org/lurker/message/20150203.014030.86a446b9.en.html "Towards systemd-free packages"
[18]: https://lists.dyne.org/lurker/message/20150215.140953.2316d1e9.en.html "Important changes in Linux 3.20 (4.0?)"
[19]: https://lists.dyne.org/lurker/thread/20150214.174603.6b3f5962.en.html "recommendation for consideration: keep as close to debian as possible"
[20]: https://git.devuan.org/devuan/devuan-project/wikis/ProjectDescription "Project Description"
[21]: https://lists.dyne.org/lurker/message/20150216.134404.35c25c45.en.html "About Devuan's audience"

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[upcoming]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/upcoming-issue "Upcoming Issue"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
