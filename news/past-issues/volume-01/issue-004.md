Author: Noel David Torres TaÃ±o

Date: 2014-12-24 02:23 -000

To: dng

Subject: [Dng] Devuan Weekly News IV


Welcome to Devuan Weekly News IV. Again this issue has come out one day
late, but Real Life (TM) still is chasing. The bad news is that this may
be the last issue.

This time I will not go for a dedicated mailing list digest, but instead,
I'll point to a single web page ([1]), which is the same as the e-mail to
this list pointed by [2].

[1] http://devuan.org/newsletter_22dec.html

[2] https://lists.dyne.org/lurker/message/20141222.172627.869b3e8e.en.html

The question is: is it worth to continue the Devuan Weekly News effort
when "the people leading the project" issue a different news page without
coordinating with DWN?

Instead of digesting for you the week on the mailing list, I want you to
think carefully and say if Devuan Weekly News

a) is useful
b) should be coordinated with such announcements

Will there be an Issue V?

Thanks for reading so far

Noel

er Envite

DWN team 