# Full disk encryption

This is an easy to follow guide to setting up full disk encryption during a Devuan install. It requires that you have completed all other steps of the install and reached the disk partitioning menu.

## Partitioning the disk

**1) Choose to use the entire disk and set up encrypted LVM. This is recommended because it's easiest  
for new users.**

![Partition with LVM and encryption](../img/crypt1.png)

&nbsp;

**2) Choose the disk that will be encrypted. Here there is only one to choose from.**

![Select a disk to encrypt](../img/crypt2.png)

&nbsp;

**3) You will need to choose a partitioning scheme as usual. Since we want to encrypt the disk the easy  
way, choose to install all files in one partition.**

![Select a partitioning scheme](../img/10part2.png)

&nbsp;

**4) You will need to write the partitioning scheme to the disk before encryption can be set up.**

![Write changes to disk](../img/crypt3.png)

&nbsp;

## Encrypting the disk

**5) The installer will now inform you that it is erasing the disk. This will take some time if you have a  
lot of disk space available.**

![Securely erasing the disk](../img/crypt4.png)

&nbsp;

**6) You should now create an encryption passphrase. For a secure passphrase I would recommend a  
minimum of 20 characters, a mixture of upper and lower case letters, at least three numbers and a  
minimum of two symbols.**

![Enter an encryption passphrase](../img/crypt5.png)

&nbsp;

**7) You will now be asked to set the size of the volume group. The simplest way is to leave this as the  
default which will use the whole disk.**

![Set size of volume](../img/crypt6.png)

&nbsp;

**8) Check the overview of changes to be written, and if you are happy with it write the changes out  
to disk.**

![Overview of changes to be written](../img/crypt7.png)

&nbsp;

**9) Finally, confirm the changes and continue with the [install](devuan-install.md).**

![Accept the changes to be made](../img/crypt8.png)

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their 
respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
