Estas instrucciones son para migrar exclusivamente desde Debian Buster. Cuando se migra a Beowulf las instrucciones son específicas para cada versión de Debian desde la que se proviene y deben seguirse al pie de la letra.

# Migrar desde Debian Buster a Beowulf

A partir de Beowulf se pierde el paquete network-manager durante la migración. La solución aquí consiste en reemplazarlo por wicd, pero si se prefiere emplear una [configuración de red](network-configuration.md) manual, será necesario hacerlo antes de continuar.

El primer paso consiste en cambiar el archivo sources.list para que apunte a los repositorios de Beowulf.

`root@debian:~# editor /etc/apt/sources.list`

Modificar el contenido para que quede como se muestra a continuación. Comentar las líneas restantes.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Actualizar la lista de paquetes desde los repositorios de Beowulf. La salida de cambios recientes en APT significa que esto no se permite, sin embargo es posible omitir este comportamiento.

`root@debian:~# apt-get update --allow-insecure-repositories`

Instalar el *keyring* de Devuan para que los paquetes puedan ser autenticados.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Actualizar las listas de paquetes nuevamente para que los paquetes sean autenticados de ahora en adelante.

`root@debian:~# apt-get update`

Si se desea utilizar el gestor de redes wicd, necesita ser instalado ahora para que la actualización no falle.

`root@debian:~# apt-get install wicd-gtk`

Actualizar los paquetes a su última versión. Notar que esto no completa la migración.

`root@debian:~# apt-get upgrade`

Una vez terminado es necesario instalar eudev. Si se está utilizando Gnome el mismo será eliminado en el proceso, pero puede ser instalado nuevamente al completar la migración.

`root@debian:~# apt-get install eudev`

Es conocido que el último comando provoca errores en paquetes, pero se solucionarán más adelante como parte del proceso de migración.

`root@debian:~# apt-get -f install`

Luego es necesario reiniciar el sistema para cambiar sysvinit a PID 1.

`root@debian:~# reboot`

Ahora es posible ejecutar la migración adecuadamente.

`root@debian:~# apt-get dist-upgrade`

Al migrar a Devuan, los paquetes relacionados con systemd ya no son necesarios.

`root@devuan:~# apt-get purge systemd libnss-systemd`

Si no se cuenta con un entorno de escritorio es posible instalar uno. Por defecto Devuan utiliza XFCE.

`root@devuan:~# apt-get install task-xfce-desktop`

O es posible instalar Gnome si se desea continuar utilizándolo.

`root@devuan:~# apt-get install task-gnome-desktop`

Finalmente se recomienda eliminar paquetes que hayan quedado huérfanos durante el proceso de migración, junto con archivos de paquetes obsoletos dejados por la instalación de Debian.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

