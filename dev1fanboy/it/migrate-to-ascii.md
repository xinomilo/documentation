# Migrare verso Devuan ASCII
Questo documento descrive la procedura per migrare da Debian Jessie o Stretch a Devuan ASCII. Al momento, la migrazione verso ASCII da Debian Jessie o Stretch è un po' più complicata se si usano GNOME o network manager a causa di qualche pacchetto obsoleto, anche se ogni procedura di migrazione relativa differisce lievemente da quella principale, dovrebbe essere possibile aggirare i problemi in entrambi i casi.

## Migrare provenendo dal desktop GNOME
Non è ancora disponibile un pacchetto di transizione, quindi gli utenti di GNOME dovranno installare manualmente xfce4 e slim. Selezionate slim come display manager predefinito quando richiesto.

`root@debian:~# apt-get install xfce4 slim`

Impostate startxfce4 come gestore di sessioni in modo che possa essere usato il nuovo desktop.

`root@debian:~# update-alternatives --config x-session-manager`

## Rimpiazzare network-manager con wicd

Per la migrazione abbiamo bisogno di wicd invece di network-manager. A causa di un problema di compatibilità con Debian, coloro che eseguono la migrazione da Stretch da remoto **dovranno** usare la [configurzione della rete](network-configuration.md) manuale, altrimenti perderanno l'accesso all'host remoto durante la procedura.

`root@debian:~# apt-get install wicd`

Assicuratevi di rimuovere gestore di rete (network-manager) dalla sequenza di avvio.

`root@debian:~# update-rc.d -f network-manager remove`

In questo modo le connessioni wireless dovrebbero essere configurate per collegarsi automaticamente con wicd. Fermate il gestore di rete (network-manager) prima di configurare la rete.

`root@debian:~# /etc/init.d/network-manager stop`

## Migrare

Devuan usa sysvinit di default quindi installiamolo.

`root@debian:~# apt-get install sysvinit-core`

È richiesto un riavvio per attribuire a sysvinit il pid1 e poter così rimuovere systemd.

`root@debian:~# reboot`

Possiamo rimuovere systemd senza proteste.

`root@debian:~# apt-get purge systemd`

Modifichiamo il file sources.list per selezionare i repository Devuan.

`root@debian:~# editor /etc/apt/sources.list`

Aggiungiamo i mirror Devuan con il ramo ASCII. Commentate il resto.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Aggiorniamo l'indice dei pacchetti così da poter installare il pacchetto "portachiavi" di Devuan.

`root@debian:~# apt-get update`

Installate il pacchetto "portachiavi" di Devuan così che i pacchetti seguenti siano autenticati.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Aggiorniamo nuovamente l'indice dei pacchetti così che siano autenticati con il "portachiavi".

`root@debian:~# apt-get update`

Finalmente possiamo migrare verso Devuan.

`root@debian:~# apt-get dist-upgrade`

## Procedure post migrazione
Rimuoviamo le componenti di systemd dal sistema.

`root@devuan:~# apt-get purge systemd-shim`

Qualora non stiate usando D-Bus o xorg potreste essere in grado di rimuovere anche libsystemd0.

`root@devuan:~# apt-get purge libsystemd0`

Rimuovete tutti i pacchetti "orfani" lasciati dalla precedente installazione Debian.

`root@devuan:~# apt-get autoremove --purge`

È il momento giusto per ripulire i vecchi pacchetti del sistema Debian.

`root@devuan:~# apt-get autoclean`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
