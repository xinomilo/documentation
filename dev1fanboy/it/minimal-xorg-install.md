# Installazione minimale di xorg
Questo documento descrive come eseguire un'installazione minimale di xorg e di alcuni buoni default.

## Installazione dei pacchetti essenziali di xorg
Prima di tutto procediamo all'installazione dell'insieme minimo dei pacchetti richiesti per xorg.

`root@devuan:~# apt-get install xserver-xorg-video-dummy xserver-xorg-input-void xinit x11-xserver-utils`

La selezione del driver dell'input void e del driver video dummy hanno la sola funzione di evitare di installare tutti i driver inclusi quelli non necessari. Descriveremo di seguito come installare solo i driver necessari.

## Installazione del driver grafico
A questo punto dovreste installare il driver grafico relativo al vostro hardware.

I driver grafici più comuni includono:

* xserver-xorg-video-intel (intel)
* xserver-xorg-video-nouveau (nvidia)
* xserver-xorg-video-openchrome (via)
* xserver-xorg-video-radeon (amd)
* xserver-xorg-video-vesa (driver grafico generico)

Per esempio se avete un chip grafico AMD dovreste installare il driver radeon.

`root@devuan:~# apt-get install xserver-xorg-video-radeon`

Se non siete sicuri del driver da utilizzare potete ricorrere al driver vesa fino a quando non avrete imparato di più riguardo ai driver xorg. Nel frattempo xorg funzionerà su tutto l'hardware conforme allo standard VESA e tornerà utile anche come alternativa di riserva in caso di problemi con il driver principale.

`root@devuan:~# apt-get install xserver-xorg-video-vesa`

Per i chip grafici non menzionati nell'elenco precedente potete cercare nel repository il driver grafico adatto al vostro hardware.

`root@devuan:~# apt-cache search xserver-xorg-video-.* | pager`

## Installazione dei driver d'input
Nella maggior parte dei casi le periferiche d'input funzioneranno senza nessun intervento grazie al driver libinput. Potete installare altri driver se avete dei requisiti specifici o se questa configurazione non vi soddisfa.

Se avete un mouse e una tastiera potete installare i relativi driver separatamente se lo preferite.

`root@devuan:~# apt-get install xserver-xorg-input-mouse xserver-xorg-input-kbd`

Nel caso abbiate un touchpad synaptics probabilmente dovrete installare i driver synaptics insieme con quelli per la tastiera.

`root@devuan:~# apt-get install xserver-xorg-input-synaptics`

In caso aveste bisogno di driver d'input diversi potrete trovarli nel repository

`root@devuan:~# apt-cache search xserver-xorg-input-.* | pager`

## Installazione extra opzionali
Si raccomanda l'installazione dei set di font di base per xorg.

`root@devuan:~# apt-get install xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable`

Gli utenti ASCII che amano i videogiochi opengl dovranno includere il supporto mesa per i driver grafici liberi su menzionati. Agli utenti Beowulf non è richiesto nessun intervento. 

`root@devuan:~# apt-get install libgl1-mesa-dri mesa-utils`

ASCII e seguenti potrebbero richiedere il pacchetto xserver-xorg-legacy per gestire i permessi del xserver. Si Noti che questo è un wrapper setuid.

`root@devuan:~# apt-get install xserver-xorg-legacy`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
