# Netzwerkkonfiguration
Manchmal möchten Sie das Netzwerk manuell konfigurieren. Dies hat den Vorteil, dass es network-manager und dbus unabhängig ist.

Für diejenigen, die remote von Stretch migrieren, müssen Sie diese Anweisungen verwenden, sonst verlieren Sie den Zugriff auf den Remote-Host.

## Kabelverbindungen
Kabelgebundene Verbindungen können verwaltet werden, indem sie in der Schnittstellenkonfigurationsdatei konfiguriert werden.

`root@debian:~# editor /etc/network/interfaces`

### Automatische Netzwerkkonfiguration
Hierdurch wird das Netzwerk automatisch konfiguriert, sobald eine Verbindung erkannt wird. Passe dies bei Bedarf entsprechend dem Namen Deiner Schnittstelle an.

~~~
allow-hotplug eth0
iface eth0 inet dhcp
~~~

### Statische Netzwerkkonfiguration
Wenn Du eine statische Netzwerkkonfiguration benötigst, passe diese an Deinen Schnittstellennamen und Dein Netzwerk an.

~~~
auto eth0
iface eth0 inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

## Drahtlose Verbindungen
Dies ist ähnlich wie bei kabelgebundenen Netzwerken, außer das Du Authentifizierungsdetails angeben musst. Wenn Du eine automatische Netzwerkkonfiguration benötigst, solltest Du den wicd-Netzwerkmanager verwenden.

Bearbeite die Schnittstellendatei, um das drahtlose Netzwerk einzurichten.

`root@debian:~# editor /etc/network/interfaces`

Passe die Schnittstellendatei an Deine Schnittstelle und Netzwerkkonfiguration an.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>