These instructions are for migrating from the Debian Stretch release only. When migrating to Beowulf the instructions are specific to the Debian release you're coming from and should be followed only as written.

# Migrate from Debian Stretch to Beowulf

It's necessary to [configure your network](network-configuration.md) before you begin, otherwise you will lose network access during the migration.

For migration from Stretch you need to install sysvinit-core before continuing.

`root@debian:~# apt-get install sysvinit-core`

To prevent hanging after a reboot libpam-systemd needs to be removed. This may remove your desktop but we will install it again later.

`root@debian:~# apt-get purge libpam-systemd`

Next change the sources.list to point to the Beowulf repositories.

`root@debian:~# editor /etc/apt/sources.list`

Make your sources.list look like the one provided. Comment out all other lines.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Now update the package lists from the Beowulf repositories.

`root@debian:~# apt-get update`

The Devuan keyring should now be installed so that packages can be authenticated.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Update the package lists again so that packages are authenticated from here on in.

`root@debian:~# apt-get update`

The libtinfo package needs to be upgraded to prevent breaks.

`root@debian:~# apt-get install libtinfo6`

Upgrade your packages so that you have the latest versions. Note that this does not complete the migration.

`root@debian:~# apt-get upgrade`

A reboot is required to change sysvinit to pid1.

`root@debian:~# reboot`

The last step before migration is to switch to eudev.

`root@debian:~# apt-get install eudev`

Now you can perform the migration. For Stretch migrations issuing a full-upgrade is recommended.

`root@debian:~# apt-get full-upgrade`

If anything breaks you need to fix it then issue a full upgrade again.

`root@debian:~# apt-get -f install`  
`root@debian:~# apt-get full-upgrade`

If you don't have a desktop at this point you should install one now. The default in Devuan is XFCE.

`root@devuan:~# apt-get install task-xfce-desktop`

Or you can install Gnome instead if you want to continue using it.

`root@devuan:~# apt-get install task-gnome-desktop`

It's now safe to remove systemd.

`root@devuan:~# apt-get purge systemd`

You can now remove any packages orphaned by the migration process, and any unusable archives left over from your Debian install.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
