# Vollständige Festplattenverschlüsselung

Dies ist eine leicht zu befolgende Anleitung zum Einrichten der vollständigen Festplattenverschlüsselung während einer Devuan-Installation. Da dies eine leicht zu befolgende Anleitung ist, wählen wir den schnellsten Weg um loszulegen.

Sie müssen zerst alle anderen Installationsschritte ausführen und den Schritt der Festplattenpartitionierung erreichen, bevor Sie fortfahren.

## Partitionierung der Festplatte

**1) Wählen Sie die gesamte Festplatte und richten Sie ein verschlüsseltes LVM ein. Dies ist der einfachste Weg dies zu tun und
empfohlen für neue Benutzer.**

![Partitionieren mit LVM und Verschlüsselung](../img/encrypt1.png)

&nbsp;

**2)Wählen Sie die Festplatte, die verschlüsselt werden soll.**

![Wählen Sie eine zu verschlüsselnde Festplatte](../img/encrypt2.png)

&nbsp;

**3) Sie müssen wie üblich ein Partitionierungsschema wählen.
Da wir den einfachsten Weg zur vollständigen Festplattenverschlüsselung wünschen, sollten Sie alle Dateien in einer Partition installieren.**

![Wählen Sie ein Partitionierungsschema](../img/partdisks2.png)

&nbsp;

**4) Sie müssen das Partitionierungsschema auf den Datenträger schreiben, bevor die Verschlüsselung eingerichtet werden kann.**

![Änderungen auf Festplatte schreiben](../img/encrypt3.png)

&nbsp;

## Verschlüsseln der Festplatte

**5) Das Installationsprogramm informiert Sie jetzt, dass es die Festplatte löscht. Dies wird einige Zeit dauern wenn
Sie viel Speicherplatz zur Verfügung haben.**

![Sicheres Löschen der Festplatte](../img/encrypt4.png)

&nbsp;

**6) Sie sollten jetzt eine Passphrase für die Verschlüsselung erstellen.
Für eine sichere Passphrase würde ich mindestens 20 Zeichen empfehlen, eine Mischung aus Groß- und Kleinbuchstaben,
mindestens drei Ziffern und mindestens zwei Symbolen.**

![Geben Sie eine Verschlüsselungspassphrase ein](../img/encrypt5.png)

&nbsp;

**7) Überprüfen Sie die Übersicht der zu schreibenden Änderungen, und wenn Sie damit zufrieden sind, schreiben Sie die Änderungen
auf die Festplatte.**

![Übersicht der zu schreibenden Änderungen](../img/encrypt6.png)

&nbsp;

**8) Bestätigen Sie abschließend die Änderungen und fahren Sie mit der Installation fort.**

![Übernehmen Sie die Änderungen](../img/encrypt7.png)

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>
