This is somewhat experimental documentation, needs some improvements to the chroot instructions.

# Creating a chrootable install environment

The purpose of this document is to show how to create an installation of Devuan that is separate from your running system. This may be useful for virtualization, or to install certain packages without altering the running system which is useful for testing purposes.

For the purposes of illustration we will refer to the Ceres (unstable) release when installing the chroot environment. In the case you want a different release substitute `ceres` for your chosen release name.

## Create the chroot install

Install the debootstrap package which we will need for the installation.

`root@devuan:~# apt-get install debootstrap`

You'll need somewhere to place chrootable system, so you should create a directory somewhere for that.

`root@devuan:~# mkdir /mnt/ceres`

Now complete the chroot installation process using debootstrap.

`root@devuan:~# debootstrap ceres http://pkgmaster.devuan.org/merged /mnt/ceres`

## Chrooting the installation

When using the chroot environment you may sometimes need to make sure `dev`, `proc` and `sys` are mounted there.

`root@devuan:~# mount -o bind /dev /mnt/ceres/dev`

`root@devuan:~# mount -o bind /sys /mnt/ceres/sys`

`root@devuan:~# mount -t proc proc /mnt/ceres/proc`

Chroot the environment to start making use of it.

`root@devuan:~# chroot /mnt/ceres /bin/bash`

## Exiting the chroot environment

When you're finished you should make sure to cleanly exit the chroot environment.

`root@chroot:~# exit`

`root@devuan:~# umount /mnt/ceres/dev`

`root@devuan:~# umount /mnt/ceres/sys`

`root@devuan:~# umount /mnt/ceres/proc`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
