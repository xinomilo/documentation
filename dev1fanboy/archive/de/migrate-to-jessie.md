# Migration zu Devuan Jessie
Dieses Dokument beschreibt, wie man von Debian zu Devuan Jessie migriert.

## Durchführen der Migration
Wir müssen die Paketquellen bearbeiten, damit wir das Devuan-Repository als Quelle für Pakete festlegen können.

`root@debian:~# editor /etc/apt/sources.list`

Ändere deine Quellen in die Devuan-Paketquellen ab, indem Du die vorherigen Debian-Quellen auskommentierst und die Devuan-Quellen hinzufügst.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Bevor wir Pakete aus dem Devuan-Repository holen können, müssen wir die Paket-Index-Dateien aktualisieren.

`root@debian:~# apt-get update`

Der Devuan Schlüsselbund ist erforderlich, um Pakete zu authentifizieren.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Nachdem der Devuan Schlüsselbund installiert wurde, solltest Du die Indizes erneut aktualisieren, damit die Pakete von nun an authentifiziert werden.

`root@debian:~# apt-get update`

Wir können jetzt nach Devuan migrieren. Wähle slim als Standard Displaymanager, wenn du dazu aufgefordert wirst.

`root@debian:~# apt-get dist-upgrade`

Um systemd als pid1 zu entfernen, ist ein einmaliger Neustart erforderlich.

`root@devuan:~# reboot`

## Aufgaben nach der Migration
Wenn Du unter Debian GNOME vor der Migration verwendet hast, empfehle ich, den Session Manager zu startxfce4 zu ändern.

`root@devuan:~# update-alternatives --config x-session-manager`

Systemd Komponenten sollten jetzt aus dem System entfernt werden.

`root@devuan:~# apt-get purge systemd systemd-shim`

Wenn Du D-Bus nicht verwendest, kannst Du libsystemd0 möglicherweise entfernen.

`root@devuan:~# apt-get purge libsystemd0`

Lösche alle verwaisten Pakete, die von der vorherigen Debian-Installation übriggeblieben sind.

`root@devuan:~# apt-get autoremove --purge`

Jetzt ist ein guter Zeitpunkt, um alte Paketarchive aus Deinem Debian-System zu entfernen.

`root@devuan:~# apt-get autoclean`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>