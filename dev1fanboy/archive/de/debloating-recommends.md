# Verschlankungsempfehlungen
In diesem Dokument wird beschrieben, wie APT so konfiguriert wird, dass Empfehlungen als optional behandelt und rückwirkend entfernt werden. Dies entfernt optionale Pakete, die in vielen Fällen unerwünscht oder ungenutzt. Danke an [TheFlash] für den Hinweis auf diese Funktion von APT.

## APT konfigurieren
Wir werden zuerst APT konfigurieren, um empfohlene Pakete als unwichtig zu behandeln.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Konfiguriere APT so, dass empfohlene Pakete als optional behandelt werden.

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

## Wichtige Pakete schützen
Während die meisten empfohlenen Pakete von unwichtiger Natur sind, gibt es einige Pakete, die vor möglicher Entfernung geschützt werden sollten.

Für die Sicherheit Deiner Browser und anderer Anwendungen sollten wir sicherstellen, dass SSL-Zertifikate immer verfügbar sind, indem Du das ca-certificates Paket installierst. Überspringe diesen Schritt nur, wenn Du weißt was Du tust.

`root@devuan:~# apt-get install ca-certificates`

Das SSL-Zertifikatspaket wird jetzt anstelle einer Abhängigkeit oder eines empfohlenen Pakets als manuell installiertes Paket markiert. Wenn der nächste Schritt Pakete zeigt, die Du behalten möchtest, kannst Du das gleiche für sie tun, bevor Du die Entfernung bestätigst.

## Entfernen unerwünschter Pakete
Du kannst nun alle empfohlenen Pakete, die durch die Konfigurationsänderungen verwaist wurden, rückwirkend entfernen.

`root@devuan:~# apt-get autoremove --purge`

Nachdem wir alle diese Pakete entfernt haben, benötigen wir sie nicht mehr im Cache.

`root@devuan:~# apt-get autoclean`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>