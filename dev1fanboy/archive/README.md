Ideally we should just prune Jessie information altogether, but I'm leaving this directory here for those who don't want to
remove their work.

Please move all Jessie specific content to this directory under your usual two letter country code directory.

Other things should not go here, the important thing is removing Jessie specific information from the main pages, and moving
Jessie specific docs here.

Note that migrating/upgrading to Ascii or Beowulf is still supported here so they do ***not*** need to be removed.
