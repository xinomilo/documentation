# Guida ad Openbox
Questa è un'introduzione ad openbox per tutti coloro che non lo hanno mai usato. Fornisce una configurazione che offre molte delle funzioni degli ambienti desktop rimanendo il più minimale possibile.

## Installazione dei pacchetti
Partiremo dall'installazione minima dei pacchetti necessari ad usare e configurare openbox.

`root@devuan:~# apt-get install openbox obconf obmenu`

Potrebbe essere utile installare il pacchetto menu in quanto fornisce gli elementi del menu per quei software che lo supportano.

`root@devuan:~# apt-get install menu`

Installeremo ora compton, il gestore della composizione del desktop.

`root@devuan:~# apt-get install compton`

Il programma idesk può essere usato per gestire le icone e gli sfondi del desktop.

`root@devuan:~# apt-get install idesk`

Come task bar adotteremo l'agile tint2.

`root@devuan:~# apt-get install tint2`

Xfe come gestore dei file è in grado di montare volumi senza un'infrastruttura di auto-mounting, a patto che abbiate configurato correttamente lo fstab.

`root@devuan:~# apt-get install xfe`

Di default openbox usa scrot per la realizzazione di screenshot quando il tasto `Print Screen` viene premuto. Basta solo installarlo.

`root@devuan:~# apt-get install scrot`

Nella sezione riguardante la configurazione di idesk useremo lo sfondo desktop di default di Devuan chiamato purpy, parte del pacchetto desktop-base.

`root@devuan:~# apt-get install desktop-base`

## Configurazione di Openbox
All'avvio di openbox vogliamo che il composite manager, desktop manager, and taskbar siano tutti disponibili. A tal fine dovremo assicurarci che la directory openbox esista in ~/.config e creare un file autostart.

`user@devuan:~$ mkdir -p ~/.config/openbox`

`user@devuan:~$ editor ~/.config/openbox/autostart`

Nel file autostart aggiungete le seguenti righe, facendo attenzione che ogni linea termini con `&` eccetto l'ultima.

~~~
compton &
idesk &
tint2
~~~

## Configurazione di idesk
Per usare idesk è necessario creare la directory usata per immagazzinare le icone del desktop oppure il programma non verrà avviato.

`user@devuan:~$ mkdir ~/.idesktop`

Avviate idesk di modo che generi la configurazione di default. Dovrete poi arrestare l'esecuzione del processo con `ctrl + c`.

`user@devuan:~$ idesk`

Modificate il file di configurazione in modo da impostare lo sfondo.

`user@devuan:~$ nano ~/.ideskrc`

In questo esempio useremo il summenzionato sfondo purpy.

~~~
Background.File: /usr/share/images/desktop-base/your-way_purpy-wide-large.png
~~~

Poi imposteremo un'icona per Xfe utilizzando il collegamento desktop di default come guida.

`user@devuan:~$ cp ~/.idesktop/default.lnk ~/.idesktop/xfe.lnk`

`user@devuan:~$ editor ~/.idesktop/xfe.lnk`

Modificate il nuovo collegamento come descritto di seguito. Ricordate di cambiare la posizione verticale (`Y:`) dell'icona in modo che non si sovrapponga a quella di default.

~~~
table Icon
  Caption: Xfe
  Command: /usr/bin/xfe
  Icon: /usr/share/pixmaps/xfe.xpm
  Width: 48
  Height: 48
  X: 30
  Y: 120
end
~~~

## Utilizzo del gestore delle sessioni di openbox
Se utilizzate un gestore grafico di login potrete autenticarvi al desktop openbox semplicemente scegliendo openbox-session come gestore delle sessioni. In caso contrario potrete usare il file di configurazione xinitrc del vostro utente ed invocare lo script startx dopo aver effettuato il login al terminale.

`user@devuan:~$ echo "exec openbox-session" > ~/.xinitrc`

`user@devuan:~$ startx`

Se lo preferite potete impostare openbox-session come gestore delle sessioni di default per tutti gli utenti.

`root@devuan:~# update-alternatives --config x-session-manager`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
