# Sgrassare i pacchetti "raccomandati"
Questa guida descrive come configurare APT in modo da considerare i pacchetti raccomandati come opzionali e rimuoverli retroattivamente. Questo processo rimuove tutti quei pacchetti opzionali che in molti casi risultano indesiderati o inutili. Un ringraziamento va a [TheFlash] per la dritta su questa funzionalità di APT.

## Configurazione di APT
Inanzitutto configuriamo APT in modo da considerare i pacchetti raccomandati come superflui.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Configuriamo APT.

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

## Conservazione dei pacchetti veramente importanti
Sebbene quasi tutti i pacchetti cosiddetti raccomandati non siano veramente importanti, ce ne sono alcuni che è bene proteggere da un'eventuale rimozione.

Per la sicurezza dei browser e di altre applicazioni, dobbiamo assicurarci che i certificati SSL rimangano sempre disponibili installando il pacchetto ca-certificates. Saltate questo passaggio a vostro rischio e pericolo.

`root@devuan:~# apt-get install ca-certificates`

I certificati SSL saranno d'ora in poi contrassegnati come installati manualmente, invece che come una dipendenza o come pacchetto raccomandato. Qualora il prossimo passaggio riveli altri pacchetti necessari o d'interesse potrete ripetere la relativa procedura prima di confermare la loro rimozione.

## Pulizia dei pacchetti indesiderati
Potrete rimuovere retroattivamente tutti i pacchetti raccomandati "orfanizzati" dalle modifiche apportate alla configurazione.

`root@devuan:~# apt-get autoremove --purge`

Adesso che sono stati rimossi, non abbiamo più bisogno che questi pacchetti siano immagazzinati nella cache.

`root@devuan:~# apt-get autoclean`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
