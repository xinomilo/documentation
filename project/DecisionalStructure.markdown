__The VUA group is in charge__, this is the collective that started the fork.

__During the initial phase__, until 1.0 is released, we shall follow a very simple conceptual line: __the more you do, the stronger your voice__ on decisions.  In case of conflict, _nextime_ and _jaromil_ are to be considered as something akin to a benevolent dictator duo.

Hopefully [after release 1.0](FutureReleases) we can try and better the structure towards a more democratic organization and carefully open decional powers outside the VUA group.  The link sends to a __DRAFT PROPOSAL__ to be discussed:

 * A core group, namely the VUA group, will have veto power on any decision
 * Any decision must be taken in strict compliance with our [Constitution](DevuanConstitution)
 * Any decision, even from the VUA group, can be pointed as non-constitutional.  In this case, the decision can pass and be considered constitutional only if 75% of VUAs agree to second it.
 * Only the VUA group can change the constitution, and this can be done only with 75% of VUAs accordance.
 * If more than 75% of Devuan developers are against a constitution change, the change will not happen.
 * All other decisions will be made in democratic ways, by open votations where the results will be calculated in different weight based on who vote with cumulative sum in case of multiple roles:
  * VUA member: weight 10 
  * Operative members ( who offer sysadmin or other work ) 3
  * developers: weight 3 up to 10 packages mantained, +1 for any other 10 packages.
  * registered users: 1
 * Quorum is established for any votation by VUAs, but can't be more than 35% of potential votes.

The VUA group can accept new members only with 75% of VUA second it.

This will be added in our [Constitution](DevuanConstitution) as a fixed ruleset after discussion and acceptance.