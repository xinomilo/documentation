# Basic concepts

Devuan is born for a simple goal: having a systemd-free debian jessie to preserve freedom choice on init and decoupling between init and the rest of the system.

Initially it will NOT be a complete fork, but just a complete infrastructure to distribute a personalized version of debian jessie, testing and unstable where some packages from us will be pinned up on top of the debian repositories, where all packages with dependencies from anything related to systemd will be removed when possible, or systemd-shim will be preferred for the others.

IF, and only IF we will be able to provide also a systemd package with no hijacking of the whole system, we will also offer it.

Following those basic guidelines, the VUA group, who started this fork, is in charge, who do things will get more decision "power", where we will be in contrast, nextime and jaromil are benevolent dictators. This is just toward 1.0 release, after that, we will try to better structure a more democratic organization, but with carefully open powers outside VUA group.

Basically i'm thinking about something like a base constitution with few points i propose later that can't be changed except if at least 75% of VUAs are ok to change it, and anything else except the basic constitution will be distributed power in a direct democratic way. VUAs will retain power on final decision to say what is constitutionally approvable or not.

Later we will better define the VUA group.

# Infrastructure

We will try to replicate as closest possible the debian infrastructure, but changes are well accepted if they improve and modernize what debian have.

The server list isn't yet disclosed for security purpose, and only core members can access the complete list in the private core project right now.

# The path towards devuan 1.0

 * build an debian_installer based ISO modified with our packages (nextime, help is welcome)
 * build base packages for devuan modification on top of jessie (devuan_basecond, corefiles and lsb) (nextime, help is welcome)
 * rebuild all packages that can be just recompiled without systemd dependecy ( help needed! )
 * profit!

This release WILL BE CALLED jessie as codename. 

Nothing else will be changed against debian jessie, except eventually from some graphics and logos.

Our primary goal is to remove systemd and to remain 100% compatible with debian jessie!
'''Our primary goal is to remove systemd and to remain 100% compatible with debian jessie!'''
'''''Our primary goal is to remove systemd and to remain 100% compatible with debian jessie!'''''

# After 1.0

# How to help for VUAs

# How to help for others

# First draft of proposed constitution